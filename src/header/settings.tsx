import * as React from "react";
import { Collector } from "tripetto-collector-rolling";
import { castToBoolean, extendImmutable } from "tripetto-collector";

export const settingsModal = (collector: Collector) => (
    <div className="modal" id="settingsModal" role="dialog" aria-labelledby="settingsModalTitle" aria-hidden="true">
        <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
                <div className="modal-header px-4">
                    <h5 className="modal-title" id="settingsModalTitle">
                        Settings
                    </h5>
                    <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="modal-body px-4">
                    <h6>Display</h6>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="setting-scrollbar"
                                type="checkbox"
                                defaultChecked={castToBoolean(collector.style.showScrollbar, true)}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                                    (collector.style = extendImmutable(collector.style, {
                                        showScrollbar: e.target.checked
                                    }))
                                }
                                className="custom-control-input"
                                aria-describedby="setting-scrollbar-label"
                            />
                            <label htmlFor="setting-scrollbar" className="custom-control-label">
                                Scrollbar
                                <small className="form-text text-secondary" id="setting-scrollbar-label">
                                    Shows the vertical scrollbar.
                                </small>
                            </label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="setting-enumerators"
                                type="checkbox"
                                defaultChecked={castToBoolean(collector.style.showEnumerators)}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                                    (collector.style = extendImmutable(collector.style, {
                                        showEnumerators: e.target.checked
                                    }))
                                }
                                className="custom-control-input"
                                aria-describedby="setting-enumerators-label"
                            />
                            <label htmlFor="setting-enumerators" className="custom-control-label">
                                Enumerators
                                <small className="form-text text-secondary" id="setting-enumerators-label">
                                    Shows question numbers for blocks that collect data.
                                </small>
                            </label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="setting-navigation"
                                type="checkbox"
                                defaultChecked={castToBoolean(collector.style.showNavigation, true)}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                                    (collector.style = extendImmutable(collector.style, {
                                        showNavigation: e.target.checked
                                    }))
                                }
                                className="custom-control-input"
                                aria-describedby="setting-navigation-label"
                            />
                            <label htmlFor="setting-navigation" className="custom-control-label">
                                Navigation bar
                                <small className="form-text text-secondary" id="setting-navigation-label">
                                    Show navigation bar.
                                </small>
                            </label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="setting-progressbar"
                                type="checkbox"
                                defaultChecked={castToBoolean(collector.style.showProgressbar, true)}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                                    (collector.style = extendImmutable(collector.style, {
                                        showProgressbar: e.target.checked
                                    }))
                                }
                                className="custom-control-input"
                                aria-describedby="setting-progressbar-label"
                            />
                            <label htmlFor="setting-progressbar" className="custom-control-label">
                                Progressbar
                                <small className="form-text text-secondary" id="setting-progressbar-label">
                                    Shows a progressbar in the navigation bar.
                                </small>
                            </label>
                        </div>
                    </div>
                    <div className="form-group">
                        <div className="custom-control custom-checkbox">
                            <input
                                id="setting-center-active-block"
                                type="checkbox"
                                defaultChecked={castToBoolean(collector.style.centerActiveBlock, true)}
                                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                                    (collector.style = extendImmutable(collector.style, {
                                        centerActiveBlock: e.target.checked
                                    }))
                                }
                                className="custom-control-input"
                                aria-describedby="setting-center-active-block-label"
                            />
                            <label htmlFor="setting-center-active-block" className="custom-control-label">
                                Center active block
                                <small className="form-text text-secondary" id="setting-center-active-block-label">
                                    Vertically center the active block.
                                </small>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);
